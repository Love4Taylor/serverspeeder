serverSpeeder
=====
### 准备工作
建议使用支持`systemd`的发行版(推荐`CentOS7`)  
确保你的内核符合要求:https://bitbucket.org/Love4Taylor/serverspeeder/raw/master/kernel-list

`CentOS6`

     rpm -ivh https://bitbucket.org/Love4Taylor/serverspeeder/raw/master/kernel/centos/kernel-firmware-2.6.32-504.3.3.el6.noarch.rpm
     rpm -ivh https://bitbucket.org/Love4Taylor/serverspeeder/raw/master/kernel/centos/kernel-2.6.32-504.3.3.el6.x86_64.rpm --force

`CentOS7`

     rpm -ivh https://bitbucket.org/Love4Taylor/serverspeeder/raw/master/kernel/centos/kernel-3.10.0-229.1.2.el7.x86_64.rpm --force

### 安装 
以`CentOS7`为例

     wget https://bitbucket.org/Love4Taylor/serverspeeder/raw/master/CraCkEd -O /bin/CraCkEd
     wget https://bitbucket.org/Love4Taylor/serverspeeder/raw/master/appex_stats -O /bin/appex_stats
     wget https://bitbucket.org/Love4Taylor/serverspeeder/raw/master/serverSpeeder.service -O /usr/lib/systemd/system/serverSpeeder.service
	 vim /usr/lib/systemd/system/serverSpeeder.service 将文件内 AmazonS3ID 和 AmazonS3KEY 修改为你的 ID 和 KEY (如何得到别问我)
     chmod +x /bin/CraCkEd
	 chmod +x /bin/appex_stats
	 systemctl daemon-reload
	 systemctl enable serverSpeeder.service

### 首次运行
进入`/`根目录

     cd /

运行`CraCkEd`并生成`config`文件夹

     CraCkEd start AmazonS3ID AmazonS3KEY

查看`appex`状态

     appex_stats

     NumOfFlows      = 733
     NumOfTcpFlows   = 732
     NumOfAccFlows   = 732
     NumOfActFlows   = 455
     LanInBytes      = 354406827948
     LanOutBytes     = 9269043579721
     WanInBytes      = 9295964951006
     WanOutBytes     = 532266411452
     LanInPackets    = 4227212196
     LanOutPackets   = 7498829564
     WanInPackets    = 7510745619
     WanOutPackets   = 7152739265
     NfIpNonLinear        = 705027142
     NfIpLinerizeFail     = 55623
     NfBypass             = 3643891128
     NfRxTcpChecksumError = 246
     NfRxRsc              = 704700486
     NfTxGso              = 18500480
     queueFullDisc0Tcp    = 1
     totalFlows = 733, totalAccFlows=732, rate=0/0(kbps), overLimit=0
     cpuId=0, curCpu=0, engId=0, cpuNum=1, ipiNum=250852674

`NumOf*`有数据代表运行正常

### 优化以及修改
优化

     cd /
     wget https://bitbucket.org/Love4Taylor/serverspeeder/raw/master/config.tar.gz
     tar zxf config.tar.gz
     rm config.tar.gz

修改加速的网卡(默认加速`eth0`)

     vim /config/appex/wanIf

其他配置都在`/config/cmds` or `/config/appex/*`
